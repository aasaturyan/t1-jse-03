# TASK MANAGER

## DEVELOPER INFO

**NAME**: Artyom Asaturyan

**E-MAIL**: aasaturyan@tech-code.ru

## SOFTWARE

**OS**: Windows 10 Professional

**JDK**: openjdk 1.8.0_322

## HARDWARE

**CPU**: AMD Ryzen 5 4600H

**RAM**: 16GB

**SSD**: 512GB

## RUN PROGRAMM

```shell
java -jar ./Application.jar
```
